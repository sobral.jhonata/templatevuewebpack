import Vue from 'vue'

Vue.directive('color', function(el, binding){
    el.style.backgroundColor = binding.value
})

export default Vue