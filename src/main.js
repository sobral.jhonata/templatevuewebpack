import Vue from 'vue'
import App from './App.vue'
import directive from '../src/directive'
import store from '../src/store'

new Vue({
    directive,
    store,
    render: h => h(App)
}).$mount('#app')