import axios from 'axios'

const BASE_URL = 'http://localhost:84'

const config = {
    baseURL: BASE_URL,
    headers: {
        'Content-Type': 'application/json;charset=utf-8'
    }
}

const httpClient = axios.create(config)

export const httpRequest = (type, url, params = null) => {
    return new Promise((resolve, reject) => {
        httpClient[type.toLowerCase()](url, params)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error)
            })
    })
}
// export default httpRequest