import Vue from 'vue'
import Vuex from 'vuex'
import state from '../store/state'
import * as getters from '../store/getters'
import * as mutations from '../store/mutations'
import * as actions from '../store/actions'

Vue.use(Vuex)

const store = new Vuex.Store({
  state,
  getters,
  actions,
  mutations
})

export default store