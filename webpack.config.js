const {
    VueLoaderPlugin
} = require('vue-loader')
const {
    HotModuleReplacementPlugin
} = require('webpack')
const HTMLWebpackPlugin = require('html-webpack-plugin')

const {
    join
} = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = {
    entry: join(__dirname, '/src/main.js'),
    output: {
        path: join(__dirname, '/dist'),
        filename: 'app.[hash].min.js'
    },
    devtool: 'source-map',
    module: {
        rules: [{
                enforce: 'pre',
                test: /\.(js|vue)$/,
                exclude: /node_modules/,
                use: 'eslint-loader'
            },
            {
                test: /\.js$/,
                use: ['babel-loader', 'eslint-loader'],
                exclude: file => (
                    /node_modules/.test(file) &&
                    !/\.vue\.js/.test(file)
                )
            },
            {
                test: /\.vue$/,
                exclude: /node_modules/,
                loader: 'vue-loader',
                options: {
                    loaders: {
                        'sass': [
                            MiniCssExtractPlugin.loader,
                            'vue-style-loader',
                            'css-loader',
                            'sass-loader'
                        ]
                    }
                }
            },
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader'
                ]
            },
            {
                test: /\.(png|jpg|gif)$/i,
                use: [{
                    loader: 'url-loader'
                }]
            }
        ]
    },
    resolve: {
        alias: {
            'vue$': 'vue/dist/vue.esm.js'
        },
        extensions: ['*', '.js', '.vue', '.json']
    },
    devServer: {
        open: true,
        hot: true
    },
    plugins: [
        new HotModuleReplacementPlugin(),
        new VueLoaderPlugin(),
        new MiniCssExtractPlugin({
            filename: 'app.[contenthash:8].min.css'
        }),
        new HTMLWebpackPlugin({
            showErrors: process.env.NODE_ENV ? true : false,
            cache: process.env.NODE_ENV ? true : false,
            template: join(__dirname, '/public/index.html')
        })
    ]
}
